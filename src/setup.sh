#!/bin/bash 

echo -e "\e[34m***************************************\e[0m"
echo "{{{{{{{{{{ SETTING UP SYSTEM }}}}}}}}}}"
echo -e "\e[34m***************************************\e[0m"

echo -e "\e[36m======================================\e[0m"
echo -e "\e[37mUpdating System\e[0m"
echo -e "\e[36m======================================\e[0m"
sudo apt -y update

echo -e "\e[36m======================================\e[0m"
echo -e "\e[37mUpgrading System\e[0m"
echo -e "\e[36m======================================\e[0m"
sudo apt -y upgrade

echo -e "\e[36m======================================\e[0m"
echo -e "\e[37mInstalling Programs\e[0m"
echo -e "\e[36m======================================\e[0m"
sudo apt -y install tmux zsh vim python3-pip curl git gnome-shell-extensions gnome-tweaks

echo -e "\e[36m======================================\e[0m"
echo -e "\e[37mSetting Up ZSH\e[0m"
echo -e "\e[36m======================================\e[0m"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended # Install Oh-My-Zsh package manager
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions # Zsh autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting # Zsh syntax highlighting
sudo chsh -s $(which zsh) # Make zsh default shell
pip install pygments # For ccat and cless

echo -e "\e[36m======================================\e[0m"
echo -e "\e[37mCopying Config Files\e[0m"
echo -e "\e[36m======================================\e[0m"
cp ./config/Gnome/.zshrc ~/
cp ./config/.vimrc ~/ 
cp ./config/.tmux.conf ~/ 

echo -e "\e[36m======================================\e[0m"
echo -e "\e[37mSetting Terminal Theme\e[0m"
echo -e "\e[36m======================================\e[0m"
dconf load /org/gnome/terminal/legacy/profiles:/ < config/Gnome/ARKOSGnomeTerminalProfile.dconf

echo -e "\e[36m======================================\e[0m"
echo -e "\e[37mSetting Icon Theme\e[0m"
echo -e "\e[36m======================================\e[0m"
mkdir -p ~/.icons
git clone https://github.com/PapirusDevelopmentTeam/papirus-icon-theme.git ~/.icons/papirus-icon-theme
cp -r ~/.icons/papirus-icon-theme/Papirus ~/.icons
cp -r ~/.icons/papirus-icon-theme/Papirus-Dark ~/.icons
echo yes | rm -r ~/.icons/papirus-icon-theme
gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Dark'

echo -e "\e[36m======================================\e[0m"
echo -e "\e[37mSetting Wallpaper\e[0m"
echo -e "\e[36m======================================\e[0m"
sudo cp wallpapers/Fedora-Wallpaper.jpg /usr/share/backgrounds
gsettings set org.gnome.desktop.background picture-uri file:////usr/share/backgrounds/Fedora-Wallpaper.jpg

echo -e "\e[36m======================================\e[0m"
echo -e "\e[37mSetting Keyboard Shortcuts\e[0m"
echo -e "\e[36m======================================\e[0m"
dconf load /org/gnome/desktop/wm/keybindings/ < config/Gnome/shortcuts/WMShortcuts
dconf load /org/gnome/mutter/wayland/keybindings/ < config/Gnome/shortcuts/WaylandShortcuts
dconf load /org/gnome/settings-daemon/plugins/media-keys/ < config/Gnome/shortcuts/MediaShortcuts
dconf load /org/gnome/shell/keybindings/ < config/Gnome/shortcuts/GnomeShellShortcuts

echo -e "\e[36m======================================\e[0m"
echo -e "\e[37mSetting Up Gnome Shell Extensions\e[0m"
echo -e "\e[36m======================================\e[0m"
mkdir -p ~/.local/share/gnome-shell/extensions/

git clone https://github.com/eonpatapon/gnome-shell-extension-caffeine.git  ~/.local/share/gnome-shell/extensions/caffeine
cp -r ~/.local/share/gnome-shell/extensions/caffeine/caffeine@patapon.info ~/.local/share/gnome-shell/extensions/
echo yes | rm -r /.local/share/gnome-shell/extensions/caffeine
dconf load /org/gnome/shell/extensions/caffeine/ < config/Gnome/extensions/caffeine
gnome-extensions enable caffeine@patapon.info

dconf load /org/gnome/shell/extensions/dash-to-dock/ < config/Gnome/extensions/dash-to-dock

git clone https://github.com/Tudmotu/gnome-shell-extension-clipboard-indicator.git  ~/.local/share/gnome-shell/extensions/clipboard-indicator@tudmotu.com
dconf load /org/gnome/shell/extensions/clipboard-indicator/ < config/Gnome/extensions/clipboard-indicator
gnome-extensions enable clipboard-indicator@tudmotu.com

git clone https://gitlab.gnome.org/GNOME/gnome-shell-extensions.git ~/.local/share/gnome-shell/extensions/user-theme@gnome-shell-extensions.gcampax.github.com
dconf load /org/gnome/shell/extensions/user-theme/ < config/Gnome/extensions/user-theme
gnome-extensions enable user-theme@gnome-shell-extensions.gcampax.github.com

git clone https://github.com/Siroj42/gnome-extension-transparent-shell.git ~/.local/share/gnome-shell/extensions/transparent-shell
cp -r ~/.local/share/gnome-shell/extensions/transparent-shell/transparent-shell@siroj42.github.io ~/.local/share/gnome-shell/extensions/
echo yes | rm -r /.local/share/gnome-shell/extensions/transparent-shell
dconf load /org/gnome/shell/extensions/transparent-shell/ < config/Gnome/extensions/transparent-shell
gnome-extensions enable transparent-shell@siroj42.github.io

git clone https://github.com/aunetx/blur-my-shell.git ~/.local/share/gnome-shell/extensions/blur-my-shell@aunetx
dconf load /org/gnome/shell/extensions/blur-my-shell/ < config/Gnome/extensions/blur-my-shell
gnome-extensions enable blur-my-shell@aunetx

test/test_setup.sh
