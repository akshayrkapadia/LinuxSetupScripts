#!/bin/bash

echo -e "\e[34m***************************************\e[0m"
echo "{{{{{{{{{{{ SECURING SYSTEM }}}}}}}}}}}"
echo -e "\e[34m***************************************\e[0m"

echo -e "\e[36m======================================\e[0m"
echo -e "\e[37mRemoving Insecure Services\e[0m"
echo -e "\e[36m======================================\e[0m"
sudo apt --purge remove xinetd nis yp-tools tftpd atftpd tftpd-hpa telnetd rsh-server rsh-redone-server

echo -e "\e[36m======================================\e[0m"
echo -e "\e[37mSecuring Shared Memory\e[0m"
echo -e "\e[36m======================================\e[0m"
echo "none /run/shm tmpfs rw,noexec,nosuid,nodev 0 0" | sudo tee -a /etc/fstab # disable permission to execute programs, change the UID of running programs, or to create block or character devices in the namespace

echo -e "\e[36m======================================\e[0m"
echo -e "\e[37mProtecting Against IP Spoofing\e[0m"
echo -e "\e[36m======================================\e[0m"
sudo cp config/sysctl.conf /etc/
# sudo cp config/host.conf /etc/

echo -e "\e[36m======================================\e[0m"
echo -e "\e[37mSetting Up Firewall\e[0m"
echo -e "\e[36m======================================\e[0m"
sudo ufw default deny incoming
sudo ufw deny telnet/tcp
sudo ufw deny ftp/tcp
sudo ufw enable

test/test_secure.sh