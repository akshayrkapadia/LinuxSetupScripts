# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Bash prompt
PS1="\\n\[\033[31m\]┌─(\[\033[33m\]\\u@\\h\[\033[31m\])─[\[\033[34m\]\\w\[\033[31m\]]\\n└──╼ \[\033[33m\]\\$ \[\033[37m\]"
export PS1

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

# ALIASES

# DNF
alias dnfi="sudo dnf install"
alias dnfr="sudo dnf remove"
alias dnfs="dnf search"
alias dnfu="sudo dnf update && sudo dnf upgrade"
alias dnfa="sudo dnf autoremove"
alias dnfc="sudo dnf clean all"
alias dnfm="dnfa && dnfc && dnfu"

# APT
alias apti="sudo apt install"
alias aptr="sudo apt remove"
alias apts="apt search"
alias aptu="sudo apt update && sudo apt upgrade"
alias apta="sudo apt autoremove"
alias aptm="apta && aptm"

# RPM-OSTREE
alias rpmosti="rpm-ostree install"
alias rpmostr="rpm-ostree uninstall"
alias rpmostu="rpm-ostree upgrade"
alias rpmosts="rpm-ostree status"

# FLATPAK
alias fpi="flatpak install"
alias fpr="flatpak uninstall"
alias fpu="flatpak update"
alias fpl="flatpak list"
alias fps="flatpak search"

# SYSTEM
#alias sysm="rpmostu && fpu && sysmfedora && sysmdev && sysmkali"
#alias sysmfedora="toolbox run -c Fedora 'sudo dnf clean all && sudo dnf autoremove && sudo dnf upadate && sudo dnf upgrade'"
#alias sysmdev="toolbox run -c Dev 'sudo dnf clean all && sudo dnf autoremove && sudo dnf upadate && sudo dnf upgrade'" 
#alias sysmkali="distobox enter Kali -- 'sudo apt clean && sudo apt autoremove && sudo apt update && sudo apt upgrade'"
#alias del="sudo rm -r"

# GIT
alias gitc="git clone"
#alias gitp="git add -A && git commit -m 'Updated' && git push origin"
alias gitp="toolbox run -c Dev cd $PWD && git add -A && git commit -m 'Updated' && git push origin"

# TOOLBOX & DISTROBOX
alias fedora="toolbox enter Fedora"
alias dev="toolbox enter Dev"
alias kali="distrobox enter Kali"

# Function that improves cd by also running ls as well
go() {
	cd "$*" && ls
}

# Start tmux on launch
#if command -v tmux &> /dev/null && [ -n "$PS1"  ] && [[ ! "$TERM" =~ screen  ]] && [[ ! "$TERM" =~ tmux  ]] && [ -z "$TMUX"  ]; then
#	exec tmux
#fi

unset rc
