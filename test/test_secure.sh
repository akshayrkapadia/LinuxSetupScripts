#!/bin/bash

echo -e "\e[34m***************************************\e[0m"
echo "{{{{{{{{{{ TESTING SECURITY }}}}}}}}}}"
echo -e "\e[34m***************************************\e[0m"

FAILURE=0

if [[ $(sudo cat /etc/fstab | grep 'none /run/shm tmpfs rw,noexec,nosuid,nodev 0 0') ]]; then
    echo -e "\e[32mSuccessfully Secured Shared Memory\e[0m"
else
    echo -e "\e[31mFailed To Secured Shared Memory\e[0m"
    FAILURE=1
fi

if [[ $(diff ../config/sysctl.conf /etc/sysctl.conf) == "" ]]; then
    echo -e "\e[32mSuccessfully Enabled IP Spoofing Protection\e[0m"
else
    echo -e "\e[31mFailed To Enable IP Spoofing Protection\e[0m"
    FAILURE=1
fi


if [[ $(sudo ufw status | grep 'active') ]]; then
    echo -e "\e[32mSuccessfully Enabled Firewall\e[0m"
else
    echo -e "\e[31mFailed To Enable Firewall\e[0m"
    FAILURE=1
fi
 
if [[ $FAILURE ]]; then
    exit 1
else
    exit 0
fi