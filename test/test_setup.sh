#!/bin/bash

echo -e "\e[34m***************************************\e[0m"
echo "{{{{{{{{{{ TESTING SETUP }}}}}}}}}}"
echo -e "\e[34m***************************************\e[0m"

FAILURE=0

if [[ $(echo $SHELL) == "/bin/zsh" ]]; then
    echo -e "\e[32mSuccessfully Set ZSH As Default Shell\e[0m"
else
    echo -e "\e[31mFailed To Set ZSH As Default Shell\e[0m"
    FAILURE=1
fi

if [[ $(diff ../config/.zshrc ~/.zshrc) == "" ]]; then
    echo -e "\e[32mSuccessfully Copied .zshrc\e[0m"
else
    echo -e "\e[31mFailed To Copy .zshrc\e[0m"
    FAILURE=1
fi

if [[ $(diff ../config/.vimrc ~/.vimrc) == "" ]]; then
    echo -e "\e[32mSuccessfully Copied .vimrc\e[0m"
else
    echo -e "\e[31mFailed To Copy .vimrc\e[0m"
    FAILURE=1
fi

if [[ $(diff ../config/.tmux.conf ~/.tmux.conf) == "" ]]; then
    echo -e "\e[32mSuccessfully Copied .tmux.conf\e[0m"
else
    echo -e "\e[31mFailed To Copy .tmux.conf\e[0m"
    FAILURE=1
fi

if [[ $(gsettings get org.gnome.desktop.background picture-uri | grep "Fedora-Wallpaper.jpg") ]]; then
    echo -e "\e[32mSuccessfully Set Wallpaper\e[0m"
else
    echo -e "\e[31mFailed To Set Wallpaper\e[0m"
    FAILURE=1
fi

dconf dump /org/gnome/desktop/wm/keybindings/ > testfile

if [[ $(diff testfile ../config/shortcuts/WMShortcuts) == "" ]]; then
    echo -e "\e[32mSuccessfully Set WM Keyboard Shortcuts\e[0m"
else
    echo -e "\e[31mFailed To Set WM Keyboard Shortcuts\e[0m"
    FAILURE=1
fi

dconf dump /org/gnome/mutter/wayland/keybindings/ > testfile

if [[ $(diff testfile ../config/shortcuts/WaylandShortcuts) == "" ]]; then
    echo -e "\e[32mSuccessfully Set Wayland Keyboard Shortcuts\e[0m"
else
    echo -e "\e[31mFailed To Set Wayland Keyboard Shortcuts\e[0m"
    FAILURE=1
fi

dconf dump /org/gnome/settings-daemon/plugins/media-keys/ > testfile

if [[ $(diff testfile ../config/shortcuts/MediaShortcuts) == "" ]]; then
    echo -e "\e[32mSuccessfully Set Media Keyboard Shortcuts\e[0m"
else
    echo -e "\e[31mFailed To Set Media Keyboard Shortcuts\e[0m"
    FAILURE=1
fi

dconf dump /org/gnome/shell/keybindings/ > testfile

if [[ $(diff testfile ../config/shortcuts/GnomeShellShortcuts) == "" ]]; then
    echo -e "\e[32mSuccessfully Set Gnome Shell Keyboard Shortcuts\e[0m"
else
    echo -e "\e[31mFailed To Set Gnome Shell Keyboard Shortcuts\e[0m"
    FAILURE=1
fi

dconf dump /org/gnome/shell/extensions/dash-to-dock/ > testfile

if [[ $(diff testfile ../config/extensions/dash-to-dock) == "" ]]; then
    echo -e "\e[32mSuccessfully Set Dock Settings\e[0m"
else
    echo -e "\e[31mFailed To Set Dock Settings\e[0m"
    FAILURE=1
fi

dconf dump /org/gnome/shell/extensions/clipboard-indicator/ > testfile

if [[ $(diff testfile ../config/extensions/clipboard-indicator) == "" ]]; then
    echo -e "\e[32mSuccessfully Set Clipboard Indicator Settings\e[0m"
else
    echo -e "\e[31mFailed To Set Clipboard Indicator Settings\e[0m"
    FAILURE=1
fi+

dconf dump /org/gnome/shell/extensions/blur-my-shell/ > testfile

if [[ $(diff testfile ../config/extensions/blur-my-shell) == "" ]]; then
    echo -e "\e[32mSuccessfully Set Blur Settings\e[0m"
else
    echo -e "\e[31mFailed To Set Blur Settings\e[0m"
    FAILURE=1
fi

dconf dump /org/gnome/shell/extensions/user-theme/ > testfile

if [[ $(diff testfile ../config/extensions/user-theme) == "" ]]; then
    echo -e "\e[32mSuccessfully Set User Theme Settings\e[0m"
else
    echo -e "\e[31mFailed To Set User Theme Settings\e[0m"
    FAILURE=1
fi

dconf dump /org/gnome/shell/extensions/transparent-shell/ > testfile

if [[ $(diff testfile ../config/extensions/transparent-shell) == "" ]]; then
    echo -e "\e[32mSuccessfully Set Transparent Shell Settings\e[0m"
else
    echo -e "\e[31mFailed To Set Transparent Shell Settings\e[0m"
    FAILURE=1
fi

dconf dump /org/gnome/shell/extensions/caffeine/ > testfile

if [[ $(diff testfile ../config/extensions/caffeine) == "" ]]; then
    echo -e "\e[32mSuccessfully Set Caffeine Settings\e[0m"
else
    echo -e "\e[31mFailed To Set Caffeine Settings\e[0m"
    FAILURE=1
fi

rm testfile

if [[ $FAILURE ]]; then
    exit 1
else
    exit 0
fi